# last updated - 180403
# sarahfong
# davis_rotation_2018

description of repo

mdd_scripts

PRS = Polygenic Risk Score
MDD = Major Depressive Disorder
PGC = Psychiatric Genomics Consortium
GWAS = Genome-Wide Association Study

Intro - The research question: "Can PRS scores for MDD inform us about Phecode patterns at different ages for patients with high and low risk for developing MDD?" 

Methods - 

(1) Generate PRS
	    PRS scores were generated at p=1.0 and p=0.05. p=1.0 assumes the GWAS is powered to calculate the probability that each SNP in the GWAS contributes to the MDD phenotype. p = 0.05 assumes the GWAS is not powered to calculate the probability that each SNP in the GWAS contributes to the MDD phenotype; instead the list of SNPs that contribute to the MDD phenotype is shortened to variants with the highest association with MDD phenotype. To increase power and PRS accuracy, we would need to increase the number of individuals in the MDD GWAS dataset.

	    MDD2018_pthresh_05.R generates PRS score at p-value threshold = 0.05
	    MDD2018_pthresh_1.R generates PRS score at p-value threshold = 1.00 

	    PRSice2 results can be found here: 
	    
	    /data/davis_lab/fongsl/mdd/p005 
	    
	    /data/davis_lab/fongsl/mdd/p10

(2) Generate Age-Specific Phecodes
    	     To run PheWAS in age-specific bins, we created age-specific phecode tables, which represent phecode tables generated from subsets of ICD codes reported for different ages.

	     There were two strategies for generating phecode tables.

	     (1) Hardcoded strategy - code in each of the age ranges and generate tables manually for each age range. This was repetitive and arduous
	     
	     180331_make_phecode_age_specific.R is a copy of 180326_make_phecode_age_specific.R, but uses a min.code.count = 1 instead of min.code.count = 2. 

	     Why did we change the min.code.count from 2 to 1? Because there are so few phecodes associated with younger patients when the min.code.count = 2, we hypothesized these younger patients were (1) not being billed for chronic conditions like older adults and therefore infrequently had 2 instances of the same billing code on the patients' Electronic Health Records (EHRs), (2) not referred for continual treatment at Vanderbilt, but coming in for single visits which would result in only one billing event on those EHRs, (3) were not receiving procedures that required a blood draw. 

	     (2) Function strategy - coded a function 'make.age.phecode' to generate age-specific phecode tables. Arguments for this function are below. This function will subset ICD code tables and generate phecode tables. The script above the make.age.phecode function MUST be run before calling the function. 

	     This script does the following:
	     
		(1) Load demography file with RUID and DOB
		(2) Load ICD file with ICD codes and date of billing
		(3) Calculates the AGE at each ICD billing event
		(4) Maps RUID to GRID for analysis in the MEGA dataset.
		(5) Declares Function  make.age.phecode <- function(min.age, max.age, min.code.count, outdir)
		(6) Generates age-specific phecode tables for the following age ranges: 0-11, 12-18, 19-25, 25-40,41-60, 61-100
		
		Age-specific phecode tables can be found here:
		/data/davis_lab/fongsl/phecode_tables/

(3) Run Age-specific PheWAS
    	
	      180419_mdd_prs_phewas.R
	      
	      This script does the following:

	        (1) Load PheWAS library, PRS scores for p-value = 1.0 and 0.05, min_code_count = 2 and 1, min_record = 100 and 25, and defines an output directory for the analysis
		(2) Scales the PRS scores
		(3) Runs a Vanilla PheWAS with Age, Gender, Ancestry, and Batch as covariates. This analysis was originally written by Jessica Dennis.
		(4) Loads the file names for age-specific phecode tables.
		(5) Declares the function 'age.phewas' 
		    
		    age.phewas <- function(phecode_table, prs, pthresh, min_code_count, min_record, outDir)

		    This will remove the age-column from the covariate file, load age-specific phecode tables, and run a PheWAS analysis for the p-value threshold declared.
		(6) Run a For-Loop where each age-specific phecode table is run as a PheWAS. It will run the analysis for each min.code.count =2, 1 phecode table and each min_record = 100, 25 threshold for phecode calling. For each P-value threshold, a separate function must be run. See example below: 
		    
		    for (pval in pval_thresh_list){
		      for (min_record in min_record_count_list){
		        for (min_code_count in min_code_count_list){
			  for (item in prs_list){
        		  

				age.phewas(prs = prs.p, 
				input = input.p, 
                   		min_code_count = min_code_count, 
                   		min_record_count = min_record, 
                   		outDir = pdir)
      }}}}