#180319
#sarahfong

#the purpose of this script is to take the results of PRSice2 analysis on Doug's psychiatric disorder datasets and to find PheWAS associations with those polygenic risk scores.


# Polygenic Risk Scores were calculated using PRSice2 with a P-value threshold set at p =1. Of course, p-value thresholds <1 can be used in future analysis, but for right now, I've kept the PRS calculation as inclusive as possible for all SNPs. This is because these psychiatric conditions are highly heterogeneous and so we make the assumption that all SNPs could carry some sort of contribution towards risk. The script running these polygenic risk scores can be found here: "/data/davis_lab/fongsl/davis_rotation_2018/180316_ruderfer_prsice.R"
library(PheWAS)

run.phewas <- function(prs, name, mega=NA, outDir) {
	   dir.create(outDir)	  
	   
	   if(mega==F){
		load('/data/davis_lab/shared/polygenic_biomarkers/bioVU_csv/phecode_tables/phecode_table_09222017.R')
		load('/data/davis_lab/shared/polygenic_biomarkers/bioVU_csv/phecode_tables/phecode_table_covariates_02162018.RData')
		colnames(prs) <- c("RUID","PRS")
		name=paste0(name, "_illumina")

		 }

	   if(mega==T){
		load('/data/davis_lab/shared/polygenic_biomarkers/bioVU_csv/phecode_tables/phecode_table_GRIDs_demo_geno_covariates_02162018.R')
		load('/data/davis_lab/shared/polygenic_biomarkers/bioVU_csv/phecode_tables/phecode_table_GRIDs_01102018.R')
		colnames(prs) <- c("GRID","PRS")
   		name=paste0(name, "_MEGA")
		}
									      
	print(paste0('MEGA=', mega))
	print('Running PheWAS')
	res <- phewas( outcomes=pheno, genotypes=prs, covariates=demo.covar, additive.genotypes=F, min.records=100, significance.threshold=c('bonferroni','fdr'), cores=6  ) 
	res_d <- addPhecodeInfo(res)
	write.table(res_d[order(res_d$p), ], file=paste0(outDir, "prs_phewas_agemedian_", name, ".txt"), col.names=T, row.names=F, quote=F, sep='\t')
									      
	print('Plotting Results')
	pdf(file=paste0(outDir, "prs_phewas_agemedian_", name, ".pdf"))
	res$phenotype <- res$outcome
	p<-phewasManhattan(res, OR.direction=T)
	print(p)
        dev.off()
																	    
}																	    
source('/data/davis_lab/dennisj/script/prs_phewas.R')

#pgc_scz<- "/data/ruderferlab/shared/bpscz2/biovu-SCZ.profile"

#dataset <- c("PsychosisInBD", "SCZvsBP", "BPvsCONT", "SCZvsCONT", "BPSCZvCONT", "suicide", "BDcondSCZ") # run 180319
dataset <-c("BDcondSCZ", "PGC2-SCZ") #run 180330
#dataset<- c("suicide")
for (i in dataset){

    # directories
    base_dir <- paste0('/data/davis_lab/fongsl/ruderfer_analysis/', i)

    #base_dir <- pgc_scz # 180329 testing results with DR

    name_illu <- paste0(i, "_pthresh_1_illumina")
    name_mega <- paste0(i, "_pthresh_1_mega")

    infile_illu <- paste0(base_dir, '/', name_illu, '/PRSice.all.score')
    infile_mega <- paste0(base_dir, '/', name_mega, '/PRSice.all.score')
    #infile_pgc_scz <- base_dir

    prs_illu <- read.table(infile_illu, h=T, stringsAsFactors=F)[, c(1,3)]
    prs_mega <- read.table(infile_mega, h=T, stringsAsFactors=F)[, c(1,3)]
    #prs_pgc <- read.table(infile_pgc_scz, h=T, stringsAsFactors=F)[, c(1,5)]
    

    out_dir = "/data/davis_lab/fongsl/ruderfer_analysis/results/phewas/"

    overlapping.ruid <- as.vector(read.table('/data/davis_lab/shared/imputed_mega/PRSice_input/overlapping_ruid.txt', h=F, stringsAsFactors=F)[,1])
    run.phewas(prs_illu[!prs_illu$FID %in% overlapping.ruid, ], name= name_illu, mega=F, outDir=out_dir)
    run.phewas(prs_illu, name= paste0(name_illu, "no_overlap_ruid"), mega=F, outDir=out_dir)
    overlapping.grid <- as.vector(read.table('/data/davis_lab/shared/imputed_mega/PRSice_input/overlapping_grid.txt', h=F, stringsAsFactors=F)[,1])
    run.phewas(prs_mega[!prs_mega$FID %in% overlapping.grid, ], name= name_mega, mega=T, outDir=out_dir)
    run.phewas(prs_mega, name= paste0(name_mega, "no_overlap_ruid"), mega=T, outDir=out_dir)
}