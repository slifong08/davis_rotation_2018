###############################################################
## Create Phecode tables (using RUID) for all individuals with phenotype data as of 1/10/2018 (n=70582)
## Create a demographic AND genetic covarate table (median age, gender, PCs, platform) for EUR individuals genotyped on Illumina platforms (n=9161)

## Create Phecode and demographic covariate files (using GRID) on all individuals with phenotype data as of 1/10/2018 (n=70582)
## Create a demographic AND genetic covariate table (median age, gender, PCs, batch) (using GRID) on all individuals genotyped on MEGA (Ruderfer lab) as of 1/10/2018

## Create dummy phenotype files on individuals with genotyping data, which are used by PRSice1 to generate a PRS on all genotyped individuals (OBSOLETE AS OF 2/16/2018 - USE WITH CAUTION)

## Write ids of samples that overlap Illumina and MEGA analyses

## created by: Jessica Dennis
## Last updated 2/16/2018
################################################################


## https://medschool.vanderbilt.edu/cpm/center-precision-medicine-blog/phewas-r-package
.libPaths(c(.libPaths(), "~/R/Rlib-3.3.3_GCC-5.4.0"))
homedir <- '/data/davis_lab/'

#phewas <- read.csv(paste0(homedir, 'shared/polygenic_biomarkers/bioVU_csv/Davis_polygenic_risk_PheWAS_codes.csv') )
#phewas file formatting would require transformations; might as well just re-create from ICD9


## Create PheWAS table from ICD codes
## The 'createPhewasTable' requires data in the form: id,icd9,count

demo.file <- read.csv(paste0(homedir, 'shared/polygenic_biomarkers/bioVU_csv/Davis_polygenic_risk_static.csv'), stringsAsFactors=F)
icd <- read.csv(paste0(homedir, 'shared/polygenic_biomarkers/bioVU_csv/Davis_polygenic_risk_icd_codes.csv'), stringsAsFactors=F)
icd <- icd[icd$ICD_FLAG==1,] #subset to ICD9 codes


################################################################
## Illumina Platforms
################################################################

  covar <- read.table(paste(homedir, 'dennisj/biomarkers/data/PRSice_input_pheno/pc_cov_02102018.txt', sep='/'), h=T)
  colnames(covar) <- c("RUID", colnames(covar)[2:length(covar)])
  demo.file <- merge(demo.file, covar, by='RUID')
  
  ##Define median age across medical record
  icd.demo <- merge(demo.file[,c("RUID","DOB")], icd, by="RUID")	
  icd.demo$AGE <- round(as.numeric(difftime(as.Date(icd.demo$ICD_DATE,format='%m/%d/%Y'), as.Date(icd.demo$DOB, format='%m/%d/%Y'), units="days"))/365.25, 2)
  
  age_median <- as.data.frame(tapply(icd.demo$AGE, icd.demo$RUID, median ))
  age_median$RUID <- rownames(age_median)
  colnames(age_median) <- c('AGE_MEDIAN','RUID')
  demo.file <- merge(demo.file[,c(1,2,5:length(demo.file))], age_median, by='RUID')
  demo.covar <- demo.file[,c(1,2,length(demo.file),3:(length(demo.file)-1))]
  save(demo.covar, file=paste0(homedir, 'shared/polygenic_biomarkers/bioVU_csv/phecode_tables/phecode_table_covariates_02162018.RData') )
  #load(paste0(homedir, 'shared/polygenic_biomarkers/bioVU_csv/phecode_tables/phecode_table_covariates_02162018.RData'))
  ## NB phecode_table_covariates_09222017.R has platform as a categorical variable, which can be problematic.
  ## The phecode_table_covaritaes_02162018.R dataset has platform dummy coded.
  
  library(plyr) #Make sure I load this package before PheWAS, to avoid conflicts with the dplyr package inherent to PheWAS
  counted <- count(icd, vars=c('RUID', 'ICD_CODE') ) #(plyr package - installed on accre)
  
  library(PheWAS)
  pheno=createPhewasTable(counted)
  save(pheno, file=paste0(homedir, 'dennisj/biomarkers/data/phecode_table_09222017.R'))
  #load(paste0(homedir, '/dennisj/biomarkers/data/phecode_table_09222017.R'))
  #copied to: /data/davis_lab/shared/polygenic_biomarkers/bioVU_csv/phecode_tables
  #load(paste0(homedir, 'shared/polygenic_biomarkers/bioVU_csv/phecode_tables/phecode_table_09222017.R'))

    
  ## If you're using a PRS as the exposure in the PheWAS, you will need to generate the PRS using a dummy phenotype file:
  ## Make a dummy phenotype file for genotyped individuals:
  ## OBSOLETE WITH PRSICE2 - IGNORE
  
  #genodir <- paste(homedir, 'shared/polygenic_biomarkers/imputed_data/IBS_filtered/PCA_outlier_removed/final_chosen_subset_files', sep='/')
  #geno.eu.ids <- as.vector(read.table(paste(genodir, 'Illumina.1kgenome.aligned.imputed.qc.cleaned.eth.EU.biallelic_only.final.ibs.filtered_final_outlier_filtered.fam', sep='/'))[,1]) #9259
  #pheno.dummy <- cbind(geno.eu.ids, rep(1, length(geno.eu.ids)))
  #write.table(pheno.dummy, file=paste(homedir, 'dennisj/biomarkers/data/PRSice_input_pheno/pheno_dummy_geno_EU.txt', sep=''), col.names=F, row.names=F, sep='\t', quote=F)

    
################################################################
## MEGA 
################################################################
  
  grid.map <- read.csv(paste0(homedir, 'shared/polygenic_biomarkers/bioVU_csv/GRID_set/Davis_Polygenetic_GRID_MAPPING.csv'))
  #mds <- read.table(paste0(homedir, 'shared/imputed_mega/mega-merged3-eur3-pos1_imputed_all/mega-pruned.mds'), h=T)
  #batch <- read.table(paste0(homedir, 'shared/imputed_mega/mega-merged3-eur3-pos1_imputed_all/mega-batch.clst'), h=F)
  #colnames(batch) <- c('FID','IID','BATCH')
  covar <- read.table(paste(homedir, 'dennisj/biomarkers/data/PRSice_input_pheno/mega_mds_batch_cov_02102018.txt', sep='/'), h=T)

  
  ##Define median age across medical record
  icd.demo <- merge(demo.file[,c("RUID","DOB")], icd, by="RUID")	
  icd.demo$AGE <- round(as.numeric(difftime(as.Date(icd.demo$ICD_DATE,format='%m/%d/%Y'), as.Date(icd.demo$DOB, format='%m/%d/%Y'), units="days"))/365.25, 2)
  
  age_median <- as.data.frame(tapply(icd.demo$AGE, icd.demo$RUID, median ))
  age_median$RUID <- rownames(age_median)
  colnames(age_median) <- c('AGE_MEDIAN','RUID')
  demo.new <- merge(demo.file[,c('RUID','GENDER')], age_median, by='RUID')
  demo.new.grid <- merge(grid.map, demo.new, by='RUID')
  demo.file <- demo.new.grid[,2:length(demo.new.grid)]
  save(demo.file, file=paste0(homedir, 'shared/polygenic_biomarkers/bioVU_csv/phecode_tables/phecode_table_GRIDs_demo_covariates_01102018.R') )
  #load(paste0(homedir, 'shared/polygenic_biomarkers/bioVU_csv/phecode_tables/phecode_table_GRIDs_demo_covariates_01102018.R'))
  
  #demo.mds <- merge(demo.file, mds[,c(2,4:length(mds))], by.x='GRID', by.y='IID')
  #cov.file <- merge(demo.mds, batch[,2:length(batch)], by.x='GRID', by.y='IID')
  demo.covar <- merge(demo.file, covar, by.x="GRID", by.y="IID")
  save(demo.covar, file=paste0(homedir, 'shared/polygenic_biomarkers/bioVU_csv/phecode_tables/phecode_table_GRIDs_demo_geno_covariates_02162018.R') )
  #load(paste0(homedir, 'shared/polygenic_biomarkers/bioVU_csv/phecode_tables/phecode_table_GRIDs_demo_geno_covariates_02162018.R'))
  ## NB phecode_table_GRIDs_demo_geno_covariates_01102018.R dataset has batch as a categorical variable, which can be problematic.
  ## The phecode_table_GRIDs_demo_geno_covariates_02162018.R dataset has platform dummy coded.

  #Create PheCODE table
  library(plyr) #Make sure I load this package before PheWAS, to avoid conflicts with the dplyr package inherent to PheWAS
  counted <- count(icd, vars=c('RUID', 'ICD_CODE') ) #(plyr package - installed on accre)
  
  ## Map to GRIDs
  count.grid <- merge(grid.map, counted, by='RUID')
  count.new <- count.grid[,2:length(count.grid)]
  
  library(PheWAS)
  pheno=createPhewasTable(count.new)
  save(pheno, file=paste0(homedir, 'shared/polygenic_biomarkers/bioVU_csv/phecode_tables/phecode_table_GRIDs_01102018.R'))
  #load(paste0(homedir, 'shared/polygenic_biomarkers/bioVU_csv/phecode_tables/phecode_table_GRIDs_01102018.R'))
  
  
  ## Make a dummy phenotype file for genotyped individuals:
  ## OBSOLETE IN PRSICE2
  
  #genodir <- paste0('/data/coxvgi/dennisj/geno/mega-merged3-eur3-pos1_imputed_all')
  #mega1.eu.ids <- as.vector(read.table(paste0(genodir, '/mega-merged3-eur3-pos1_imputed_all.fam'), stringsAsFactors = F))[,1] #18948
  #pheno.dummy <- cbind(mega1.eu.ids, rep(1, length(mega1.eu.ids)))
  #write.table(pheno.dummy, file=paste(homedir, 'shared/imputed_mega/PRSice_input/mega-merged3-eur3-pos1_imputed_all_pheno_dummy.txt', sep=''), col.names=F, row.names=F, sep='\t', quote=F)
  
  
## write file containing ids of overlapping samples
 
  ## Retrieve subject IDs with clean genotyping data
    genodir <- paste(homedir, 'shared/polygenic_biomarkers/imputed_data/IBS_filtered/PCA_outlier_removed/final_chosen_subset_files', sep='/')
    geno.eu.ids <- as.vector(read.table(paste(genodir, 'Illumina.1kgenome.aligned.imputed.qc.cleaned.eth.EU.biallelic_only.final.ibs.filtered_final_outlier_filtered.fam', sep='/'))[,1]) #9259

  ## Retrieve subject IDs with clean genotyping (MEGA) data
    genodir <- paste0(homedir, '/shared/imputed_mega/mega-merged3-eur3-pos1_imputed_all')
    mega1.eu.fam <- read.table(paste0(genodir, '/mega-merged3-eur3-pos1_imputed_all_filtered_rel_pruned.fam'), stringsAsFactors=F) #18445
  	grid.map <- read.csv(paste0(homedir, 'shared/polygenic_biomarkers/bioVU_csv/GRID_set/Davis_Polygenetic_GRID_MAPPING.csv'), stringsAsFactors=F)
  	mega1.eu.map <- merge(grid.map, mega1.eu.fam, by.x="GRID", by.y="V1")
  	table(is.element(mega1.eu.map$RUID, geno.eu.ids))
  	#FALSE  TRUE 
  	#16202  2183 
  	
  ## write file containing GRID of overlapping samples
  	overlap.ids <- mega1.eu.map[mega1.eu.map$RUID %in% geno.eu.ids, "GRID"]
  	write.table(cbind(overlap.ids, overlap.ids), file=paste0(homedir, '/shared/imputed_mega/PRSice_input/overlapping_grid.txt'), col.names=F, row.names=F, quote=F, sep='\t')
  	overlap.ids <- mega1.eu.map[mega1.eu.map$RUID %in% geno.eu.ids, "RUID"]
  	write.table(cbind(overlap.ids, overlap.ids), file=paste0(homedir, '/shared/imputed_mega/PRSice_input/overlapping_ruid.txt'), col.names=F, row.names=F, quote=F, sep='\t')



  